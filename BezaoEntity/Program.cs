﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BezaoDBEntity.EF;
using BezaoEntity.EF;
using BezaoEntity.Models;

namespace BezaoEntity
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Entity Framework Practice");
            int userName = AddNewRecord();
            // Console.WriteLine(userName);
            // RemoveUser("Dara");
            //QueryingWithSql();
            // QueryingWithLinq();
            //UpdateUser(1);
            //PrintAllUsers();



            //Database.SetInitializer(new MyDataInitializer());
            //Console.WriteLine("***** Fun with ADO.NET EF Code First *****\n");
            //using (var context = new AutoLotEntities())
            //{
            //    foreach (DBUser c in context.DBUsers)
            //    {
            //        Console.WriteLine(c);
            //    }
            //}
            //Console.ReadLine();

        }

        private static void QueryingWithSql()
        {
            using(var context = new AutoLotEntities())
            {
                try
                {
                    var banned = new BannedUsers() { Name = "Okoro", Id = 5 };
                    foreach (BannedUsers c in context.Database.SqlQuery(typeof(BannedUsers), "Select Id, Name from Users "))
                    {
                        Console.WriteLine(c);
                    }
                }catch(Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
        }

        private static void QueryingWithLinq()

        {
            using(var context = new AutoLotEntities())
            {
                foreach (User c in context.Users.Where(c => c.Name == "Alex"))
                {
                    Console.WriteLine(c.Name);
                    Console.WriteLine(c.Id);
                }
            }
           
        }
        private static int AddNewRecord()
        {
            using (var context = new AutoLotEntities())
            {
                try
                {
                    var user = new User() { Name = "baby", Email = "baby@gmail.com" };
                    context.Users.Add(user);
                    context.SaveChanges();
                    return user.Id;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    return 0;
                }
            }
        }

        private static void RemoveUser(string Name)
        {
            using(var context = new AutoLotEntities())
            {
                User deleteuser = context.Users.Find(Name);

                if(deleteuser != null)
                {
                    context.Users.Remove(deleteuser);

                    if(context.Entry(deleteuser).State != System.Data.Entity.EntityState.Deleted)
                    {
                        throw new Exception("Unable to delete record");
                    }
                    context.SaveChanges();
                }
            }
        }


        private static void UpdateUser(int Id)
        {
           
            using (var context = new AutoLotEntities())
            {
              
                User userToUpdate = context.Users.Find(Id);
                if (userToUpdate != null)
                {
                    Console.WriteLine(context.Entry(userToUpdate).State);
                    userToUpdate.Name = "Sage The Unruly";
                    Console.WriteLine(context.Entry(userToUpdate).State);
                    context.SaveChanges();
                }
            }
        }
                    
       private static void PrintAllUsers()
        {
            using(var context = new AutoLotEntities())
            {
                foreach(User c in context.Users)
                {
                    Console.WriteLine(c.Name);
                }
            }
        }
        
    }

    
}
